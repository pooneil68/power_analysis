clear all

dn = [100 1000 10000];
mu0 = 0;
mu = [-0.4:0.01:-0.11 -0.10:0.0025:0.10 0.11:0.01:0.4]; 
sd = 1;
alpha = 0.05;
itr = 1000;

result = NaN(itr, length(mu), length(dn));
for kk=1:length(dn)
tic;
for jj=1:length(mu)
for ii=1:itr
    d = mu(jj) + sd * randn(dn(kk),1);
    result(ii,jj,kk) = ttest(d, mu0, alpha);
end
end
toc;
end

p1 = sum(result(:,:,1),1)/itr;
p2 = sum(result(:,:,2),1)/itr;
p3 = sum(result(:,:,3),1)/itr;

figure
hold on
h1 = plot(mu, p1, 'mo-');
h2 = plot(mu, p2, 'go-');
h3 = plot(mu, p3, 'ko-');
plot([mu(1) mu(end)], [0.05 0.05], 'k--')
legend(gca, '100', '1000', '10000', 'Location', 'West')
legend(gca, 'boxoff')

% Elapsed time is 46.348083 seconds.
% Elapsed time is 51.609616 seconds.
% Elapsed time is 107.214269 seconds.
% save('result4.mat', 'result')
% print -dpng result4

return

