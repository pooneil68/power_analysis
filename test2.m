clear all

dn = [5 10 100];
mu0 = 0;
mu = [-3:0.1:-0.6 -0.5:0.025:0.5 0.6:0.1:3]; 
sd = 1;
alpha = 0.05;
itr = 10000;

result = NaN(itr, length(mu), length(dn));
for kk=1:length(dn)
tic;
for jj=1:length(mu)
for ii=1:itr
    d = mu(jj) + sd * randn(dn(kk),1);
    result(ii,jj,kk) = ttest(d, mu0, alpha);
end
end
toc;
end

p1 = sum(result(:,:,1),1)/itr;
p2 = sum(result(:,:,2),1)/itr;
p3 = sum(result(:,:,3),1)/itr;

figure
hold on
plot(mu, p1, 'mo-')
plot(mu, p2, 'go-')
plot(mu, p3, 'ko-')
plot([-3 3], [0.05 0.05], 'k--')

% save('result3.mat', 'result')
% print -dpng result3

% itr = 1000;
% mu = [-3:0.1:3]; 
% Elapsed time is 19.870329 seconds.
% Elapsed time is 20.285844 seconds.
% Elapsed time is 20.375435 seconds.
% save('result1.mat', 'result')

% itr = 1000;
% mu = [-3:0.1:-0.6 -0.5:0.025:0.5 0.6:0.1:3]; 
% Elapsed time is 29.037413 seconds.
% Elapsed time is 28.885725 seconds.
% Elapsed time is 29.261054 seconds.
% save('result2.mat', 'result')

% itr = 10000;
% mu = [-3:0.1:-0.6 -0.5:0.025:0.5 0.6:0.1:3]; 
% Elapsed time is 296.385217 seconds.
% Elapsed time is 292.687581 seconds.
% Elapsed time is 293.750732 seconds.
% save('result3.mat', 'result')

return

