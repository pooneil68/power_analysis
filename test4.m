clear all


load result3
itr = 10000;
p1 = sum(result(:,:,1),1)/itr;
p2 = sum(result(:,:,2),1)/itr;
p3 = sum(result(:,:,3),1)/itr;
mu = [-3:0.1:-0.6 -0.5:0.025:0.5 0.6:0.1:3]; 

figure
set(gcf, 'PaperPosition', [2 2 6 6])

subplot(2,1,1)
hold on
h1 = plot(mu, p1, 'mo-');
h2 = plot(mu, p2, 'go-');
h3 = plot(mu, p3, 'ko-');
plot([mu(1) mu(end)], [0.05 0.05], 'k--')
legend(gca, '5', '10', '100', 'Location', 'West')
legend(gca, 'boxoff')
axis([-Inf Inf 0 1.01])

load result4
itr = 1000;
p1 = sum(result(:,:,1),1)/itr;
p2 = sum(result(:,:,2),1)/itr;
p3 = sum(result(:,:,3),1)/itr;
mu = [-0.4:0.01:-0.11 -0.10:0.0025:0.10 0.11:0.01:0.4]; 

subplot(2,1,2)
hold on
h1 = plot(mu, p1, 'ko-');
h2 = plot(mu, p2, 'ro-');
h3 = plot(mu, p3, 'bo-');
plot([mu(1) mu(end)], [0.05 0.05], 'k--')
legend(gca, '100', '1000', '10000', 'Location', 'West')
legend(gca, 'boxoff')
axis([-Inf Inf 0 1.01])

% print -dpng result5


ef = [...
5 1.70
10 1.00
100 0.285
1000 0.090
10000 0.027
];

figure
set(gcf, 'PaperPosition', [2 2 4 4])
plot(log10(ef(:,1)),log10(ef(:,2)), 'o-')
axis equal
axis([0 4.1 -2 0.5])
set(gca, 'box','off')
lx = get(gca, 'xlabel');
set(lx,'string', 'log10(number of sampled data)');
ly = get(gca, 'ylabel');
set(ly,'string', 'log10(effect size)');

% figure
% plot(log10(ef(:,1)),normcdf(ef(:,2),0,1), 'o-')




