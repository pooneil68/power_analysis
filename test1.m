clear all

dn = 100;
mu0 = 0;
mu = [-0.5:0.1:0.5]; 
sd = 1;
alpha = 0.05;
itr = 10000;

result = NaN(itr, 4);
for jj=1:length(mu)
for ii=1:itr
    d = mu(jj) + sd * randn(dn,1);
    result(ii,jj) = ttest(d, mu0, alpha);
end
end
p = sum(result,1)/itr


return

% データの数
dn = 100;
mu0 = 0; sd0 = 1;
mu1 = 1; sd1 = 1;
mu2 = 0.1; sd2 = 1;
alpha = 0.05;
itr = 10000;

result0 = NaN(itr, 4);
for ii=1:itr
    d0 = mu0 + sd0 * randn(dn,1);
    [H,P,CI] = ttest(d0, mu0, alpha);
    result0(ii,:) = [H P CI'];
end
p0 = sum(result0(:,1))/itr

result1 = NaN(itr, 4);
for ii=1:itr
    d1 = mu1 + sd1 * randn(dn,1);
    [H,P,CI] = ttest(d1, mu0, alpha);
    result1(ii,:) = [H P CI'];
end
p1 = sum(result1(:,1))/itr

result2 = NaN(itr, 4);
for ii=1:itr
    d2 = mu2 + sd2 * randn(dn,1);
    [H,P,CI] = ttest(d2, mu0, alpha);
    result2(ii,:) = [H P CI'];
end
p2 = sum(result2(:,1))/itr


return



figure

bin = -4:0.1:4;
subplot(2,1,1)
hist(d0, bin)
subplot(2,1,2)
hist(d1, bin)

